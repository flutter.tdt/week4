mixin CommonValidator {
  String? validateEmail(String? value) {
    if (!value!.contains('@')) {
      return 'Please input an valid email.';
    }
    return null;
  }
  String? validatePassword(String? value) {
    if (value!.length < 5) {
      return "Password must be at least 5 characters.";
    }
    return null;
  }
}