import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Log Me In!',
      home: Scaffold(
        body: LoginScreen(),
      )
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }

}

class LoginScreenState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 10),),
            passwordField(),
            Container(margin: EdgeInsets.only(top: 10),),
            loginButton(),
          ],
        ),
      )
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email address'
      ),
      validator: (String? value) {
        if (!value!.contains('@')) {
          return 'Please input an valid email.';
        }
        return null;
      },
      onSaved: (String? value) {
        email = value as String;
      },
    );
  }

  Widget passwordField() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Password'
      ),
      validator: (String? value) {
        if (value!.length < 4) {
          return "Password must be at least 4 characters.";
        }
        return null;
      },
      onSaved: (String? value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();
            print('Save $email, $password');
          }
        },
        child: Text('Login')
    );
  }

}